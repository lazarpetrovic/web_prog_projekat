import flask
from flask import Blueprint

from utils.db import mysql

login_blueprint = Blueprint("login_blueprint", __name__)

@login_blueprint.route("/login", methods=["POST"])
def login():
    login_korisnik = flask.request.json
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM korisnik WHERE korisnicko_ime=%(korisnicko_ime)s AND lozinka=%(lozinka)s", flask.request.json)
    korisnik = cursor.fetchone()
    if korisnik is not None:
        flask.session["korisnik"] = korisnik # Dodavanje podataka o korisniku u sesiju.
        return flask.jsonify({"success": True}), 200
    return flask.jsonify({"success": False}), 404

@login_blueprint.route("/logout", methods=["GET"])
def logout():
    flask.session.pop("korisnik", None) # Odjava sa sistema vrsi se uklanjanjem korisnika iz sesije.
    return flask.jsonify({"success": True}), 200

@login_blueprint.route("/currentUser", methods=["GET"])
def current_user():
    if session.get("korisnik") is not None:
        login_korisnik = flask.request.json
        cursor = mysql.get_db().cursor()
        cursor.execute("SELECT * FROM korisnik WHERE id=%s", (session.get("korisnik")["id"]))
        korisnik = cursor.fetchone()
    else:
        return "", 400
    #return flask.jsonify(flask.session.get("korisnik")), 200

@login_blueprint.route("/isLoggedIn", methods=["GET"])
def isLoggedIn():
    return flask.jsonify(flask.session.get("korisnik") is not None)