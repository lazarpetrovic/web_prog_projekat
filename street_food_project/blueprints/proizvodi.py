import flask
from flask.blueprints import Blueprint

from utils.db import mysql

proizvodi_blueprint = Blueprint("proizvodi_blueprint", __name__)

@proizvodi_blueprint.route("/proizvodi", methods=["GET"])
def dobavi_proizvode():
    if flask.session.get("korisnik") is None:
        return "", 403
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM proizvod")
    proizvodi = cursor.fetchall()
    return flask.jsonify(proizvodi)

@proizvodi_blueprint.route("/proizvodi/<int:id>", methods=["GET"])
def dobavi_proizvod(id):
    if flask.session.get("korisnik") is None:
        return "", 403
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM proizvod WHERE id=%s", (id))
    proizvod = cursor.fetchone()
    return flask.jsonify(proizvod)
    

@proizvodi_blueprint.route("/dodajProizvod", methods=["POST"])
def dodaj_proizvod():
    if flask.session.get("korisnik") is None:
        return "", 403
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("INSERT INTO proizvod(naziv, sastojci, cena, opis_proizvoda, tip_proizvoda_id) VALUES(%(naziv)s, %(sastojci)s, %(cena)s, %(opis_proizvoda)s, %(tip_proizvoda_id)s)", flask.request.json)
    db.commit()
    return flask.jsonify(flask.request.json)

@proizvodi_blueprint.route("/proizvodi/<int:id>", methods=["DELETE"])
def ukloni_proizvod(id):
    if flask.session.get("korisnik") is None:
        return "", 403
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("DELETE FROM proizvod WHERE id=%s", (id,))
    db.commit()
    return ""

@proizvodi_blueprint.route("/proizvodi/<int:id>", methods=["PUT"])
def izmeni_proizvod(id):
    if flask.session.get("korisnik") is None:
        return "", 403
    db = mysql.get_db()
    cursor = db.cursor()
    data = flask.request.json
    data["id"] = id
    print(id)
    cursor.execute("UPDATE proizvod SET naziv=%(naziv)s, sastojci=%(sastojci)s, cena=%(cena)s, opis_proizvoda=%(opis_proizvoda)s, tip_proizvoda_id=%(tip_proizvoda_id)s WHERE id=%(id)s", data)
    db.commit()
    return "", 200