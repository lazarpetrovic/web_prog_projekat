(function(angular){
    var app = angular.module("app")

    app.controller("LoginCtrl", ["loginService", "$state", "$scope", function(loginService, $state, $scope){
        var that = this;
        this.status = "neprijavljen";
        this.failed = false;
        //this.admin = false;
        this.ulogovan = false;
        this.korisnik = {
            "korisnicko_ime": "",
            "lozinka":"",
        }

        var onLogin = function() {
            that.ulogovan = true;
        }        

        var onLogout = function() {
            that.ulogovan = false;
        }

        loginService.addLoginListener($scope, onLogin);
        loginService.addLogoutListener($scope, onLogout);

        that.login = function() {
            loginService.login(that.korisnik, function(){
                that.ulogovan = true;
                console.log(that.ulogovan);
                $state.go('pocetna');
                //that.status = "prijavljen";
                //console.log(that.admin);
            },
            function(){
                that.failed = true;
                that.status = "neprijavljen";
                window.alert("Podaci koje ste uneli ne postoje u bazi, pokusajte ponovo!");
            })
        }

        that.logout = function(){
            loginService.logout(function(){
                //that.admin = false;
                $state.go('login');
                //that.status = "neprijavljen"
            },
            function(){

            })
        };

        that.currentUser = function() {
        loginService.isLoggedIn(function(){
            that.ulogovan = true;
            //that.status = "prijavljen";
            $state.go('pocetna');
        },
        function(){
            that.ulogovan = false;
            //that.status = "neprijavljen";
            
        })
        };

        /*this.login = function() {
            this.prijavljen = true;
            $http.post("api/login", this.korisnik).then(
                function(response) {
                    that.status = "prijavljen";
                    //console.log("ulogovao sam se")
                    console.log(that.korisnik)
                    //console.log(that.status)
                    //window.location.reload("/pocetna")
                    $state.go("pocetna");
                    //window.location.reload()
                    that.korisnik = {
                        korisnicko_ime: "",
                        lozinka: "",
                    }
                    
                },
                function(reason){
                    that.status = "neprijavljen";
                    window.alert("Podaci koje ste uneli ne postoje u bazi, pokusajte ponovo!")
                    that.korisnik = {
                        korisnicko_ime: "",
                        lozinka: ""
                    }
                }
            )
        }

        this.logout = function() {
            $http.get("api/logout").then(
                function(response) {
                    that.status = "neprijavljen";
                    that.prijavljen = false;
                    //console.log("izlogovao sam se")
                    //console.log(that.korisnik)
                    //console.log(that.status)
                    //window.location.reload()
                    that.korisnik = {
                        korisnicko_ime: "",
                        lozinka: "",
                    }
                    $state.go("home");
                    
                },
                function(reason) {
                    console.log(reason);
                }
            )
        }

        this.getCurrentUser = function() {
            $http.get("api/currentUser").then(
                function(response) {
                    if(response.data) {
                        that.status = "prijavljen";
                    } else {
                        that.status = "neprijavljen";
                    }
                },
                function(reason) {
                    console.log(reason);
                }
            )
        }
        */
        this.currentUser();
    }]);
})(angular);