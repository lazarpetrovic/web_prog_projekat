(function(angular){
    var app = angular.module("app");

    app.controller("ProizvodCtrl", ["$http", "$stateParams", "$state", function($http, $stateParams, $state){
        var that = this;
        this.proizvod = {};

        this.dobaviProizvod = function(id) {
            $http.get("api/proizvodi/" + id).then(function(result){
                that.proizvod = result.data;
            },function(reason){
                console.log(reason);
            })
        }
        this.dobaviProizvod($stateParams["id"])
    }]);
})(angular)