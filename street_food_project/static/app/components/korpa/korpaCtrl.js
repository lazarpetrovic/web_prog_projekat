(function(angular){
    var app = angular.module("app");

    app.controller("KorpaCtrl", ["$http", "$state", "$scope", function($http, $state, $scope){
        var that = this;

        this.proizvodi = [];

        this.dobaviProizvode = function() {
            $http.get("api/proizvodi").then(function(result){
                console.log(result);
                that.proizvodi = result.data;
            },function(reason){
                console.log(reason);
                if(reason.status == 403){
                   $state.go("login");
                }
            })
        }
    }])
})(angular)