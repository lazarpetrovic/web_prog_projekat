(function(angular){
    var app = angular.module("app");

    app.controller("KorisniciCtrl", ["$http", "$scope", "$stateParams", "$state", function($http, $scope, $stateParams, $state){
        var that = this;
        //this.korisnik = {};
        this.proizvodi = [];
        this.korisnici = [];
        this.noviKorisnik = {
            "korisnicko_ime":"",
            "lozinka":"",
            "ime":"",
            "prezime":"",
        }

        this.dobaviKorisnike = function() {
            $http.get("api/korisnici").then(function(result){
                console.log(result);
                that.korisnici = result.data;
            }, function(reason){
                console.log(reason);
            });
        }

        this.pronadjen = 'nepronadjen';

        this.dodajKorisnika = function() {
            var pronadjen = false;
            for(var i = 0; i < that.korisnici.length; i++){
                if(that.korisnici[i]['korisnicko_ime'] == this.noviKorisnik.korisnicko_ime){
                    pronadjen = true;
                }
            }

            if(pronadjen == false){
                $http.post("api/korisnici", that.noviKorisnik).then(function(response){
                    console.log(response);
                    that.pronadjen = 'pronadjen';
                    $state.go("login");
                }, function(reason){
                    console.log(reason);
                })
            }
            else{
                window.alert("Korisnik sa unetim korisnickim imenom vec postoji, pokusajte ponovo!");
            }
            
        }

        this.obrisiKorisnika = function(id) {
            $http.delete("api/korisnici/" + id).then(function(response){
                console.log(response);
                that.dobaviKorisnike();
            },
            function(reason){
                console.log(reason);
            })
        }

        this.dobaviKorisnika = function(id) {
            $http.get("api/korisnik/" + id).then(function(result){
                that.noviKorisnik = result.data;
            },function(reason){
                console.log(reason);
            })
        }

        this.izmeniKorisnika = function(id) {
            $http.put("api/korisnik/" + id, that.noviKorisnik).then(function(response){
                console.log(response);
                $state.go("korisnik", {"id": id});
            },
            function(reason){
                console.log(reason);
                //if(reason.status == 403){
                //    $state.go("login");
                //}
            })
        }

        this.sacuvaj = function() {
            if($stateParams["id"]) {
                this.izmeniKorisnika($stateParams["id"], that.korisnik);
            } else {
                this.dodajKorisnika();
            }
        }

        if($stateParams["id"]){
            this.dobaviKorisnika($stateParams["id"]);
        }

        this.dobaviKorisnike();
    }]);
})(angular);