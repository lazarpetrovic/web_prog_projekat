(function(angular){
    var app = angular.module("app")

    app.controller("dodavanjeCtrl", ["$http", "$state", "$stateParams", function($http, $state, $stateParams){
        var that = this;

        this.kategorije = [];

        this.noviProizvod = {
            "naziv":"",
            "sastojci":"",
            "cena": 0,
            "opis_ proizvoda":"",
            "tip_proizvoda_id": null
        }

        this.dobaviKategorije = function(){
            $http.get("api/kategorije").then(function(result){
                console.log(result);
                that.kategorije = result.data;

                that.noviProizvod.tip_proizvoda_id = that.kategorije[0].id;
                
            })
        }

        this.dodajProizvod = function() {
            $http.post("api/dodajProizvod", this.noviProizvod).then(function(response){
                console.log(response);
                $state.go("pocetna");
            },function(reason){
                console.log(reason);
                if(reason.status == 403) {
                    $state.go("login");
                }
            })
        }

        this.dobaviProizvod = function(id) {
            $http.get("api/proizvodi/" + id).then(function(result){
                that.noviProizvod = result.data;
                that.noviProizvod["tip_proizvoda_id"] = that.noviProizvod["tip_proizvoda_id"];
            }, function(reason){
                console.log(reason);
                if(reason.status == 403){
                    $state.go("login");
                 }
            })
        }

        this.izmenaProizvoda = function(id) {
            $http.put("api/proizvodi/" + id, that.noviProizvod).then(function(response){
                console.log(response);
                $state.go("proizvod", {"id": id});
            },function(reason){
                console.log(reason);
                if(reason.status == 403){
                    $state.go("login");
                 }
            })
        }

        this.sacuvaj = function(){
            if($stateParams["id"]) {
                this.izmenaProizvoda($stateParams["id"], that.proizvod);
            } else {
                this.dodajProizvod();
            }
        }

        if($stateParams["id"]){
           this.dobaviProizvod($stateParams["id"]);
        }

        this.dobaviKategorije();

    }])
})(angular)