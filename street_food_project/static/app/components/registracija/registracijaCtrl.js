(function(angular){
    var app = angular.module("app")

    app.controller("RegistracijaCtrl", ["$http", "$state", "$stateParams", function($http, $state, $stateParams){
        var that = this;
        this.korisnik = {};
        this.korisnici = [];
        this.noviKorisnik = {
            "korisnicko_ime":"",
            "lozinka":"",
            "ime":"",
            "prezime":"",
            "tip_korisnika_id": null
        }

        this.dobaviKorisnike = function() {
            $http.get("api/korisnici").then(function(result){
                console.log(result);
                that.korisnici = result.data;
            }, function(reason){
                console.log(reason);
            });
        }

        this.pronadjen = 'nepronadjen';

        this.dodajKorisnika = function() {
            var pronadjen = false;
            for(var i = 0; i < that.korisnici.length; i++){
                if(that.korisnici[i]['korisnicko_ime'] == this.noviKorisnik.korisnicko_ime){
                    pronadjen = true;
                }
            }

            if(pronadjen == false){
                $http.post("api/korisnici", that.noviKorisnik).then(function(response){
                    console.log(response);
                    that.pronadjen = 'pronadjen';
                    $state.go("login");
                }, function(reason){
                    console.log(reason);
                })
            }
            else{
                window.alert("Korisnik sa unetim korisnickim imenom vec postoji, pokusajte ponovo!");
            }
            
        }

        this.dobaviKorisnike();
    }])
})(angular)