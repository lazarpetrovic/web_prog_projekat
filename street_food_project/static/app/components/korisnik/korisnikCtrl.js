(function(angular){
    var app = angular.module("app");

    app.controller("KorisnikCtrl", ["$http", "$stateParams", "$state", function($http, $stateParams, $state){
        var that = this;
        this.korisnik = {};

        this.dobaviKorisnika = function(id) {
            $http.get("api/korisnik/" + id).then(function(result){
                that.korisnik = result.data;
            },function(reason){
                console.log(reason);
            })
        }

        this.showPassword = function(){
            var sifra = document.getElementById("passwordInput");
            if (sifra.type === "password") {
                sifra.type = "text";
            } else {
                sifra.type = "password";
            }
        }
        this.dobaviKorisnika($stateParams["id"])
    }]);
})(angular);