(function(angular){
    var app = angular.module("app");

    app.controller("ProizvodiCtrl", ["$http", "$state", function($http, $state){
        var that = this;

        this.proizvodi = []

        this.dobaviProizvode = function() {
            $http.get("api/proizvodi").then(function(result){
                console.log(result);
                that.proizvodi = result.data;
            },function(reason){
                console.log(reason);
                if(reason.status == 403){
                    $state.go("login");
                 }
            })
        }

        this.obrisiProizvod = function(id) {
            $http.delete("api/proizvodi/" + id).then(function(response){
                console.log(response);
                that.dobaviProizvode();
            }, function(reason){
                console.log(reason);
                if(reason.status == 403){
                    $state.go("login");
                 }
            })
        }

        

        this.dobaviProizvode();
    }])
})(angular)